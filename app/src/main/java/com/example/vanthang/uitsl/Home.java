package com.example.vanthang.uitsl;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.example.vanthang.uitsl.Model.Control;
import com.example.vanthang.uitsl.Model.Data;
import com.example.vanthang.uitsl.Utils.Common;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.jetbrains.annotations.NotNull;

import io.ghyeok.stickyswitch.widget.StickySwitch;
import pl.pawelkleczkowski.customgauge.CustomGauge;

public class Home extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    TextView txt_name_user, txt_temperature, txt_light, txt_rain,txt_humidity;
    CustomGauge gauge1, gauge2, gauge3,gauge4;
    StickySwitch btn_control;

    FirebaseDatabase db;
    DatabaseReference data,control;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("UIT SL");
        toolbar.setBackgroundColor(Color.BLACK);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        //init database
        db = FirebaseDatabase.getInstance();
        data = db.getReference("Data").child(Common.current_User.getPhone());
        control=db.getReference("Data").child(Common.current_User.getPhone()).child("Control");


        //set header view
        View headerView = navigationView.getHeaderView(0);
        txt_name_user = headerView.findViewById(R.id.txt_full_name);

        txt_name_user.setText(Common.current_User.getName());
        txt_name_user.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/NABILA.TTF"));

        //View
        txt_temperature = findViewById(R.id.txt_temperature);
        txt_light = findViewById(R.id.txt_light);
        txt_rain = findViewById(R.id.txt_rain);
        txt_humidity=findViewById(R.id.txt_humidity);

        gauge1 = findViewById(R.id.gauge1);
        gauge2 = findViewById(R.id.gauge2);
        gauge3 = findViewById(R.id.gauge3);
        gauge4=findViewById(R.id.gauge4);

        btn_control = findViewById(R.id.btn_control);

        gauge1.setEndValue(125);
        gauge2.setEndValue(3200);
        gauge3.setEndValue(100);
        gauge4.setEndValue(100);

        loadData();
        btn_control.setOnSelectedChangeListener(new StickySwitch.OnSelectedChangeListener() {
            @Override
            public void onSelectedChange(@NotNull StickySwitch.Direction direction, @NotNull String s) {
                if (btn_control.getDirection() == StickySwitch.Direction.LEFT) {
                    control.setValue("0");
                    Toast.makeText(Home.this, "Sào đang thu !", Toast.LENGTH_SHORT).show();
                } else if (btn_control.getDirection() == StickySwitch.Direction.RIGHT) {
                    control.setValue("1");
                    Toast.makeText(Home.this, "Sào đang giương !", Toast.LENGTH_SHORT).show();
                }
            }
        });


    }
    private void loadData() {
        data.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Data data = dataSnapshot.getValue(Data.class);
                gauge1.setValue(Math.round(Float.parseFloat(data.getTemperature())));
                gauge2.setValue(Math.round(Float.parseFloat(data.getLight())));
                gauge3.setValue(Math.round(Float.parseFloat(data.getRain())));
                gauge4.setValue(Math.round(Float.parseFloat(data.getHumidity())));

                txt_temperature.setText(new StringBuilder(data.getTemperature()).append("  C"));
                txt_light.setText(new StringBuilder(data.getLight()).append(" lx"));
                txt_rain.setText(new StringBuilder(data.getRain()).append(" %"));
                txt_humidity.setText(new StringBuilder(data.getHumidity()).append(" %"));

                if (data.getControl().equals(String.valueOf(0)))
                    btn_control.setDirection(StickySwitch.Direction.LEFT);
                else
                    btn_control.setDirection(StickySwitch.Direction.RIGHT);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.d("ANT", databaseError.getMessage());
            }
        });
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {
        } else if (id == R.id.nav_about) {
            startActivity(new Intent(Home.this, About.class));
        } else if (id == R.id.nav_exit) {
            startActivity(new Intent(Home.this, MainActivity.class));
            finish();
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    protected void onResume() {
        loadData();
        super.onResume();
    }

}
