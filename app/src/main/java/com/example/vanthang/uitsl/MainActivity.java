package com.example.vanthang.uitsl;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    Button btn_signin,btn_signup;
    TextView txt_slogan;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //view
        btn_signin=findViewById(R.id.btn_signin);
        btn_signup=findViewById(R.id.btn_signup);
        txt_slogan=findViewById(R.id.txt_slogan);

        Typeface typeface=Typeface.createFromAsset(getAssets(),"fonts/NABILA.TTF");
        txt_slogan.setTypeface(typeface);

        btn_signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this,SignIn.class));
            }
        });
        btn_signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this,SignUp.class));
            }
        });
    }
}
