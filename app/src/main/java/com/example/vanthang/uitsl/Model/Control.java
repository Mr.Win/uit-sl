package com.example.vanthang.uitsl.Model;

public class Control {
    private String Control;

    public Control() {
    }

    public Control(String control) {
        Control = control;
    }

    public String getControl() {
        return Control;
    }

    public void setControl(String control) {
        Control = control;
    }
}
