package com.example.vanthang.uitsl.Model;

public class Data {
    private String Temperature;
    private String Light;
    private String Rain;
    private String Humidity;
    private String Control;

    public Data() {
    }

    public Data(String temperature, String light, String rain, String humidity, String control) {
        Temperature = temperature;
        Light = light;
        Rain = rain;
        Humidity = humidity;
        Control = control;
    }

    public String getTemperature() {
        return Temperature;
    }

    public void setTemperature(String temperature) {
        Temperature = temperature;
    }

    public String getLight() {
        return Light;
    }

    public void setLight(String light) {
        Light = light;
    }

    public String getRain() {
        return Rain;
    }

    public void setRain(String rain) {
        Rain = rain;
    }

    public String getHumidity() {
        return Humidity;
    }

    public void setHumidity(String humidity) {
        Humidity = humidity;
    }

    public String getControl() {
        return Control;
    }

    public void setControl(String control) {
        Control = control;
    }
}
