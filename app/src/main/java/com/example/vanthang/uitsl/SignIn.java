package com.example.vanthang.uitsl;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.vanthang.uitsl.Model.User;
import com.example.vanthang.uitsl.Utils.Common;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.rengwuxian.materialedittext.MaterialEditText;

import dmax.dialog.SpotsDialog;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class SignIn extends AppCompatActivity {
    MaterialEditText edt_phone,edt_password;
    Button btn_signin;

    FirebaseDatabase db;
    DatabaseReference users;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //before setcontentview
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/Arkhip_font.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build());
        setContentView(R.layout.activity_sign_in);

        //init database
        db = FirebaseDatabase.getInstance();
        users=db.getReference("Users");

        //view
        edt_phone=findViewById(R.id.edt_phone);
        edt_password=findViewById(R.id.edt_password);
        btn_signin=findViewById(R.id.btn_signin);

        btn_signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final android.app.AlertDialog waitingdialog=new SpotsDialog.Builder().setContext(SignIn.this).build();
                waitingdialog.setMessage("Please waiting");
                waitingdialog.show();
                users.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        waitingdialog.dismiss();
                        //check if user not exists in database
                        if (dataSnapshot.child(edt_phone.getText().toString()).exists())
                        {
                            //Get user information
                            User user=dataSnapshot.child(edt_phone.getText().toString()).getValue(User.class);
                            if (user.getPassword().equals(edt_password.getText().toString()))
                            {
                                Toast.makeText(SignIn.this, "Sign in successful", Toast.LENGTH_SHORT).show();
                                Common.current_User=user;
                                startActivity(new Intent(SignIn.this,Home.class));
                                finish();
                            }
                            else {
                                Toast.makeText(SignIn.this, "Password incorrect", Toast.LENGTH_SHORT).show();
                            }
                        }else {
                            Toast.makeText(SignIn.this, "User not exists", Toast.LENGTH_SHORT).show();
                        }

                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        waitingdialog.dismiss();
                        Log.d("ANT",databaseError.getMessage());
                    }
                });
            }
        });
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
