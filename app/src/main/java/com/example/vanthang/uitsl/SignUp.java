package com.example.vanthang.uitsl;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.vanthang.uitsl.Model.User;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.rengwuxian.materialedittext.MaterialEditText;

import dmax.dialog.SpotsDialog;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class SignUp extends AppCompatActivity {
    MaterialEditText edt_phone,edt_name,edt_password;
    Button btn_SignUp;

    FirebaseDatabase db;
    DatabaseReference users;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //before setcontentview
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/Arkhip_font.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build());
        setContentView(R.layout.activity_sign_up);

        //View
        edt_phone=findViewById(R.id.edt_phone);
        edt_name=findViewById(R.id.edt_name);
        edt_password=findViewById(R.id.edt_password);
        btn_SignUp=findViewById(R.id.btn_signup);

        //init database
        db = FirebaseDatabase.getInstance();
        users=db.getReference("Users");

        btn_SignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final android.app.AlertDialog waitingdialog=new SpotsDialog.Builder().setContext(SignUp.this).build();
                waitingdialog.setMessage("Please waiting");
                waitingdialog.show();

                users.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        //Check if already user phone
                        if (dataSnapshot.child(edt_phone.getText().toString()).exists())
                        {
                            waitingdialog.dismiss();
                            Toast.makeText(SignUp.this, "Phone number already register", Toast.LENGTH_SHORT).show();
                        }else {
                            waitingdialog.dismiss();
                            User user=new User(edt_name.getText().toString(),edt_password.getText().toString(),edt_phone.getText().toString());
                            users.child(edt_phone.getText().toString()).setValue(user);
                            Toast.makeText(SignUp.this, "Sign up successful !", Toast.LENGTH_SHORT).show();
                            finish();
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        Log.d("ANT",databaseError.getMessage());
                    }
                });
            }
        });
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
